<?php
  // Generates a list of videos
  session_start();
  require_once '../classes/video.php';
  require_once '../include/db.php';
  if (isset($_GET['query'])) {  // Show videos which contains the query value
    $video->createVideoListTable("SELECT id, name, description FROM videos WHERE name LIKE ? OR description LIKE ? ORDER BY tstamp DESC ", array ('%'.$_GET['query'].'%', '%'.$_GET['query'].'%'));
  } else {
    $video->createVideoListTable("SELECT id, name, description FROM videos ORDER BY tstamp DESC LIMIT 10", array ());
  }

?>
