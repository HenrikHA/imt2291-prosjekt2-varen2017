<?php
// Script is called to log out a user, transfers the user back to index.php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';
header ("Location: index.php");
?>
<script>
	window.location = 'index.php';
</script>
