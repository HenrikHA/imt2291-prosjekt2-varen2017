<?php

//session_start();
//require_once '../classes/video.php';
//require_once '../include/db.php';

 ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		        	<h3 class="panel-title">Nye videoer</h3>
		        </div>
	            <div class="panel-body" id="newVideos">
<?php
                if (isset($_GET['query'])) {  // Show videos which contains the query value
                  $video->createVideoListTable("SELECT id, name, description FROM videos WHERE name LIKE ? OR description LIKE ? ORDER BY tstamp DESC ", array ('%'.$_GET['query'].'%', '%'.$_GET['query'].'%'));
                } else {
                  $video->createVideoListTable("SELECT id, name, description FROM videos ORDER BY tstamp DESC LIMIT 10", array ());
                }
                ?>
	           	</div>
		    </div>
		</div>
		<div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		        	<h3 class="panel-title">Nye spillelister</h3>
		        </div>
	            <div class="panel-body" id="newPlaylists">
	           	</div>
		    </div>
		</div>
	</div>
</div>
