<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		        	<h3 class="panel-title">Mine videoer</h3>
		        </div>
	            <div class="panel-body">
	            	<?php
	            		require_once 'classes/video.php';
                  // Henter liste over brukerens egne videoer
	            		$video->createVideoListTable("SELECT id, owner_id, name, description FROM videos WHERE owner_id=? ORDER BY name", array ($user->getUID()));
	            	?>
	           	</div>
		    </div>
		</div>
		<div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
              <div class="container-fluid" style="float: right;margin-top:-9px;">
                <form class="form-inline" method="post" action="index.php">
                  <div class="form-group">
                    <input type="text" class="form-control" name="newPlaylistName" id="newPlaylistName" placeholder="Navn på spilleliste">
                  </div>
                  <button type="submit" class="btn btn-default">Opprett spilleliste</button>
                </form>
              </div>
		        	<h3 class="panel-title">Mine spillelister</h3>
		        </div>
	            <div class="panel-body">
                <?php
                  require_once ('classes/playlist.php');
                  // Henter brukerens egne spillelister
                  $playlist->generatePlaylist ('SELECT id, owner_id, name FROM playlists WHERE owner_id=? ORDER BY name', array ($user->getUID()));
                ?>
	           	</div>
		    </div>
		</div>
	</div>
</div>
