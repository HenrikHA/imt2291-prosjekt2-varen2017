<?php
$_apache = apache_request_headers();	// Ekstra informasjon ligger i disse, må hentes spesielt
$fn = (isset($_apache['X_FILENAME']) ? $_apache['X_FILENAME'] : false);

$fn = tempnam(getcwd() . "/../tmp", "VID_");
$name = substr($fn, strrpos ($fn, DIRECTORY_SEPARATOR)+1);

header ("Content-type: application/json");	// Vi sender svaret som json data
if ($fn) {									// Dersom en fil er mottatt

	// AJAX call
	file_put_contents(						// Ta i mot filen og lagre den i uploads katalogen
		$fn,												// Her må en ta høyde for dupliserte filnavn
		file_get_contents('php://input')
	);
	echo json_encode(array('ok'=>'OK', 'filename'=>$fn, 'name'=>$name));	// Send svar til klienten
}
