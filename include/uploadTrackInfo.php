<style type="text/css">
	.fileUpload {
	    position: relative;
	    overflow: hidden;
	    margin: 10px;
	}
	.fileUpload input.upload {
	    position: absolute;
	    top: 0;
	    right: 0;
	    margin: 0;
	    padding: 0;
	    font-size: 20px;
	    cursor: pointer;
	    opacity: 0;
	    filter: alpha(opacity=0);
	}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title">Last opp en fil med teksting</h3></div>
    <div class="panel-body" style="margin-top: 10px;">
		<form method="post" action="editVideo.php?video=<?php echo $_GET['video']; ?>" enctype="multipart/form-data">
			<div class="form-group">
			  	<label for="kind">Type</label>
			  	<select name="kind" class="form-control" placeholder="Hvilken type annotering/teksting laster du opp" value="subtitles">
			  		<option value="subtitles">Undertekst (standard)</option>
			  		<option value="caption">Inneholder all dialog og lydeffekter (hørselshemmede)</option>
			  		<option value="chapters">Inneholder kapittelnavn (passer for å kunne navigere i filmen)</option>
			  		<option value="descriptions">Inneholder en tekstlig beskrivelse av innholdet (synshemmede)</option>
			  		<option value="metadata">Inneholder data for datamaskinen (ikke synlig for brukeren)</option>
			  	</select>
			</div>
			<div class="form-group">
			    <label for="label">Navn</label>
			    <input required type="text" id="label" name="label" class="form-control" placeholder="Navnet/tittelen på sporet">
			</div>
			<div class="form-group">
			    <label for="srclang">Språk</label>
			    <input required type="text" id="srclang" name="srclang" class="form-control" placeholder="no - for norsk, en - for engelsk">
			</div>
			<div class="checkbox">
			    <label>
			    	<input type="checkbox" name="default" value="true"> Default (bruk som standard)
			    </label>
			</div>
			<div class="form-group">
				<input id="uploadFile" class="form-control" style="width: 80%; float: left;" placeholder="Velg fil" disabled="disabled" />
				<div class="fileUpload btn btn-primary" style="margin-top:0px">
					<span>Velg fil</span>
					<input required name="file" type="file" id="file" class="upload" />
				</div>
			</div>
			<div class="form-group">
				<input type="submit" name="addTrack" class="btn btn-primary" value="Last opp fil"/>
			</div>
		</form>
	</div>
</div>
<script>
$(function () {
	$('#file').on('change', function(event) {
		$('#uploadFile').val($('#file').val());
	});
});
</script>
<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title">Endre en eksisterende tekstfil</h3></div>
    <div class="panel-body" style="margin-top: 10px;">
		<form method="post" action="editVideo.php?video=<?php echo $_GET['video']; ?>">
			<div class="form-group">
				<label for="subtitleTrack">Velg tekstfil</label>
				<select name="subtitleTrack" id="subtitleTrackID" class="form-control" value="subtitles">
						<option value="new">new</option>
				</select>
					<script>
					$(function() {
								$("track").each(function() {
							  $('#subtitleTrackID').append($("<option></option>").attr("value", $("track").attr("id")) .text($("track").attr("label")) .attr("text", $("track").attr("src")));
						  });
					});
					</script>
			</div>
			<div class="form-group">
			    <label for="label">Navn</label>
			    <input required type="text" id="tekstTittel" name="label" class="form-control" placeholder="Navnet/tittelen på sporet">
			</div>
			<script>
			$(function () {
				$('#subtitleTrackID').on('change', function(e) {
						$("#tekstTittel").attr("value", $("#subtitleTrackID :selected").text());
			});
			});
			</script>

			<div class="form-group">
				<label for="label">Innhold</label>

			</div>
					<div class="form-group">
			  	<label for="kind">Type</label>

			  	<select name="kind" class="form-control" placeholder="Hvilken type annotering/teksting laster du opp" value="subtitles">
			  		<option value="subtitles">Undertekst (standard)</option>
			  		<option value="caption">Inneholder all dialog og lydeffekter (hørselshemmede)</option>
			  		<option value="chapters">Inneholder kapittelnavn (passer for å kunne navigere i filmen)</option>
			  		<option value="descriptions">Inneholder en tekstlig beskrivelse av innholdet (synshemmede)</option>
			  		<option value="metadata">Inneholder data for datamaskinen (ikke synlig for brukeren)</option>
			  	</select>

					<div class="form-group">
				    <label for="srclang">Språk</label>
				    <input required type="text" id="srclang" name="srclang" class="form-control" placeholder="no - for norsk, en - for engelsk">
					</div>
					<div class="checkbox">
			    	<label>
			    		<input type="checkbox" name="default" value="true"> Default (bruk som standard)
			    	</label>
					</div>
			<div class="form-group">
				<label for="editor">Tekst editor</label>
				<div name="editor">
						<input type="button" id="currentTimeBtn" value="Insert current time"/>
						<input type="button" id="headerBtn" value="Insert header"/>
						<input type="button" id="arrowBtn" value="Insert arrow"/>
						<input type="button" id="imageBtn" value="Insert img tag"/>
						<textarea id="editorText" name="editorText" draggable="false" style="width: 100%; height: 300px; float: left;"></textarea>
				</div>
			</div>
			</div>
			<div class="form-group">
				<input type="hidden" name="id" id="hiddenId" value="" />
				<input type="submit" name="editTrack" class="btn btn-primary" value="Last opp fil"/>
			</div>
		</form>
	</div>
</div>


<script>
$(function () {
 $('#subtitleTrackID').on('change', function(e) {
			$.get($("#subtitleTrackID :selected").attr("text"), function(data){
							$("#editorText").val(data);
			});
			var track = $("#subtitleTrackID :selected").attr("text");
			var id = track.substr(track.indexOf("=") + 1);
				$('#hiddenId').val(id);
 });
});

jQuery("#currentTimeBtn").on('click', function() {
			var $txt = jQuery("#editorText");
			var caretPos = $txt[0].selectionStart;
			var textAreaTxt = $txt.val();
			var curTime = parent.document.getElementById("video").currentTime;
			var date = new Date(null);
			date.setSeconds(curTime);
			var txtToAdd = date.toISOString().substr(11, 12);
			$txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
});

jQuery("#headerBtn").on('click', function() {
			var $txt = jQuery("#editorText");
			var caretPos = $txt[0].selectionStart;
			var textAreaTxt = $txt.val();
			var txtToAdd = "WEBVTT\n";
			$txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
});

jQuery("#arrowBtn").on('click', function() {
			var $txt = jQuery("#editorText");
			var caretPos = $txt[0].selectionStart;
			var textAreaTxt = $txt.val();
			var txtToAdd = " --> ";
			$txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
});

jQuery("#imageBtn").on('click', function() {
			var $txt = jQuery("#editorText");
			var caretPos = $txt[0].selectionStart;
			var textAreaTxt = $txt.val();
			var txtToAdd = "img:";
			$txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
});
</script>
