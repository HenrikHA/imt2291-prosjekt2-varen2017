<?php
session_start();

// Includes
require_once 'include/db.php';
require_once 'classes/user.php';

 ?>
<DOCTYPE html>
 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta name="description" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Prosjekt 1 - WWW-Teknologi</title>

     <link rel="apple-touch-icon" href="images/favicon.png">
     <link rel="icon" type="image/png" href="images/favicon.png">
     <!-- Place favicon.ico in the root directory -->

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
     <link rel="stylesheet" href="styles/main.css">
     <link rel="stylesheet" href="player/css/style.css" />
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

   </head>
   <body>
     <nav class="navbar navbar-default">
       <div class="container-fluid" id="navbar">
       </div><!-- /.container-fluid -->
     </nav>
 <?php

 if (isset($_GET['video'])) {    // En video er valgt med via GET med navn video
             include_once ('include/video.php');    // Viser innholdet for avspillingssiden for video

         } /*else if ($user->isLoggedIn()) {   // Bruker er logget inn og blir sendt til sitt panel som viser egne videoer og spillelister
             include_once ('include/userPanel.php');
         } else {    // Besøkende får listet opp de nyeste videoene og spillelistene
             include_once ('include/newVideos.php');
         }
*/

 ?>

  <section id="mainContent">

  </section>

  <script>
 $( document ).ready(function(){
   document.title = "Prosjekt2 - WWW-Teknologi";

   // First time load get topmenu
   updateNavbar();
   <?php

 if (!isset($_GET['video'])) {

   ?>
   // First time load, show new videos
   $('#mainContent').load('resources.php?action=include/newVideos.php');
 <?php } ?>

 });

// Refreshes navbar and enable onclick function
 function updateNavbar() {
   // Update topmenu
   syncAjax('resources.php?action=include/topMenu.php', '#navbar');

   $('#bs-example-navbar-collapse-1 a').click(function() {

     // Get data from 'data-path' and insert into '#mainContent'
     syncAjax($(this).data("path"), '#mainContent');
   });
 }

// Execute synchronous ajax request to 'url' and insert successfull response into 'hook'
function syncAjax(url, hook) {
  $.ajax({
    url: url,
    success: function(data){
      $(hook).html(data);
      //console.log(data);
    },
    error: function(error){
      console.log('syncAjax error: ', error);
    },
    async: false
  });
}


 </script>

 </body>
</html>
