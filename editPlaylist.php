<?php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';
?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Prosjekt 1 - WWW-Teknologi</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php
        require_once 'include/topMenu.php';
        if (isset($_POST['title'])) {   // The user is updating the playlist
            $sql = "UPDATE playlists set name=? WHERE owner_id=? and id=?";
            $sth = $db->prepare ($sql);
            $sth->execute (array ($_POST['title'], $user->getUID(), $_GET['id']));
            foreach ($_POST['video'] as $vid => $operation) {   // Go trough all videos in the playlist
                  if ($operation === "toTop") {    // Move this video to the top of the list
                    $sql = "SELECT sorder FROM map_videos_playlists WHERE pid=? and vid=?";
                    $sth = $db->prepare ($sql);
                    $sth->execute (array ($_GET['id'], $vid));
                    if ($row = $sth->fetch()) {
                        $sql = "UPDATE map_videos_playlists set sorder = sorder+1 WHERE pid=? and sorder < ?; UPDATE map_videos_playlists set sorder=? WHERE vid=? and pid=?";
                        $sth = $db->prepare ($sql);
                        $sth->execute (array ($_GET['id'], $row['sorder'], 1, $vid, $_GET['id']));
                    }
                } else if ($operation === "toBottom") { // Move this video to the bottom of the list
                    $sql = "SELECT sorder FROM map_videos_playlists WHERE pid=? and vid=?";
                    $sth = $db->prepare ($sql);
                    $sth->execute (array ($_GET['id'], $vid));
                    if ($row = $sth->fetch()) {
                        $sql = "UPDATE map_videos_playlists set sorder = sorder-1 WHERE pid=? and sorder > ?";
                        $sth = $db->prepare ($sql);
                        $sth->execute (array ($_GET['id'], $row['sorder']));
                        $sql = "SELECT MAX(sorder) as sorder FROM map_videos_playlists WHERE pid=?";
                        $sth = $db->prepare ($sql);
                        $sth->execute (array ($_GET['id']));
                        $sql = "UPDATE map_videos_playlists set sorder = ? WHERE vid=? and pid=?";
                        $sth = $db->prepare ($sql);
                        $sth->execute (array ($row['sorder']+1, $vid, $_GET['id']));
                    }
                }
            }
        }

        if (isset($_GET['id'])) {
            // Show form for editing title and description ?>
            <form id="editForm" method="POST" action="<?php echo $_SERVER['PHP_SELF'].'?id='.$_GET['id']; ?>">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Videoer i spillelisten : <?php
                        $owner = -1;
                        $sql = "SELECT name,  owner_id FROM playlists WHERE id=?";
                        $sth = $db->prepare ($sql);
                        $sth->execute (array ($_GET['id']));
                        if ($row = $sth->fetch()) {
                            echo "<input type='text' name='title' value='{$row['name']}' style='width:70%'/>";
                            $owner = $row['owner_id'];
                        }
                    ?></h3>
                </div>
                <div class="panel-body">

                    <input type="submit" value="Lagre tittel" class="btn btn-primary" style="margin-top: 10px"/>

                <?php
                    require_once 'classes/video.php';
                    // Create list of videos in playlist
                    $video->createVideoListTable("SELECT id, name  FROM videos, map_videos_playlists WHERE pid=? and vid=id ORDER BY sorder", array ($_GET['id']));
                ?>
                </div>
            </div>
            </form><?php
        }

        if ($user->isLoggedIn()) { ?>
            <script>
                $(function () {
                    // Each td element with the description contains a data-id attribute
                    // Add a drop down to the upper right part of that td with options
                    // to move to top, move to bottom and remove video from playlist.
                    $('td[data-id]').each (function(idx, el) {
                        var html = '<span style="float:right"><SELECT name="video[';
                        html += el.dataset.id+']"';
                        html += '><option value=""></option><option value="toTop" title="Flytt denne videoen til toppen av spillelisten">&#8657;</option>';
                        html += '<option value="remove" title="Slett denne videoen fra spillelisten">X</option>';
                        html += '<option value="toBottom" title="Flytt denne videoen til bunnen av spillelisten">&#8659;</option></select></span>';
                        el.innerHTML = html+el.innerHTML;
                    });
                    // If the user makes a selection in any drop down then auto submit the form
                    $('select').change (function() {
                        $('#editForm')[0].submit();
                    })
                });
            </script> <?php
        }
    ?>

  </body>
</html>
