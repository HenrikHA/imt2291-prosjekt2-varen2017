-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28. Feb, 2017 14:20 p.m.
-- Server-versjon: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prosjekt1`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `map_videos_playlists`
--

CREATE TABLE `map_videos_playlists` (
  `pid` bigint(20) NOT NULL,
  `vid` bigint(20) NOT NULL,
  `sorder` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `map_userviewed_videos`
--

CREATE TABLE `map_userviewed_videos` (
  `uid` bigint(20) NOT NULL,
  `vid` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `persistantlogin`
--

CREATE TABLE `persistantlogin` (
  `uid` bigint(20) NOT NULL,
  `identifier` char(32) COLLATE utf8_bin NOT NULL,
  `token` char(32) COLLATE utf8_bin NOT NULL,
  `tstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `playlists`
--

CREATE TABLE `playlists` (
  `id` bigint(20) NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `tstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `owner_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `password` char(255) COLLATE utf8_bin NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `firstname` varchar(128) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(128) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `videoadditions`
--

CREATE TABLE `videoadditions` (
  `id` bigint(20) NOT NULL,
  `mime` varchar(255) COLLATE utf8_bin NOT NULL,
  `extras` text COLLATE utf8_bin NOT NULL COMMENT 'Extra information, json encoded',
  `content` mediumblob NOT NULL COMMENT 'text track',
  `vid` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) NOT NULL,
  `filename` varchar(128) COLLATE utf8_bin NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `tstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `duration` float DEFAULT NULL,
  `mimetype` varchar(128) COLLATE utf8_bin NOT NULL,
  `owner_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `map_videos_playlists`
--
ALTER TABLE `map_videos_playlists`
  ADD PRIMARY KEY (`pid`,`vid`),
  ADD KEY `fk_vid` (`vid`);

--
-- Indexes for table `persistantlogin`
--
ALTER TABLE `persistantlogin`
  ADD UNIQUE KEY `uid_identifier` (`uid`,`identifier`);

--
-- Indexes for table `playlists`
--
ALTER TABLE `playlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_OwnPlay` (`owner_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `videoadditions`
--
ALTER TABLE `videoadditions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vidExtras` (`vid`,`extras`(255)),
  ADD KEY `fk_videoTrack` (`vid`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_OwnVid` (`owner_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `playlists`
--
ALTER TABLE `playlists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `videoadditions`
--
ALTER TABLE `videoadditions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `map_videos_playlists`
--
ALTER TABLE `map_videos_playlists`
  ADD CONSTRAINT `fk_pid` FOREIGN KEY (`pid`) REFERENCES `playlists` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_vid` FOREIGN KEY (`vid`) REFERENCES `videos` (`id`) ON DELETE CASCADE;

--
-- Begrensninger for tabell `playlists`
--
ALTER TABLE `playlists`
  ADD CONSTRAINT `fk_OwnPlay` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Begrensninger for tabell `videoadditions`
--
ALTER TABLE `videoadditions`
  ADD CONSTRAINT `fk_videoTrack` FOREIGN KEY (`vid`) REFERENCES `videos` (`id`) ON DELETE CASCADE;

--
-- Begrensninger for tabell `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `fk_OwnVid` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

INSERT INTO `users` (email, password, admin, firstname, lastname)
VALUES ('admin@admin.com','$2y$10$d2uBJzzP4ScMtnSnzEgO7eHkdFWfZ57KrTaXR69Zp1xHh1vZ9oe92','1','Test','Admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
