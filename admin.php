<?php
//session_start();
require_once 'include/db.php';
require_once 'classes/user.php';

if ($user->isAdministrator()) {     // Only available if user is admin
  if (isset($_POST['addUser'])) { // Add a user
    $res = $user->addUser($_POST['newEmail'], $_POST['newPassword'], $_POST['newFirstname'], $_POST['newLastname']);
    if (isset($res['success'])) { // Succesfully added ?>
      <div class="alert alert-success" role="alert">
        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
        <span class="sr-only">Bruker opprettet:</span>
        Ny bruker er opprettet og kan ta i bruk systemet
      </div> <?php
    } else { // Failed adding user ?>
      <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Feil:</span>
        Kunne ikke opprette ny bruker, finnes e-post adressen registrert allerede?
      </div> <?php
    }
  } else if (isset($_GET['deleteUser'])) { // remove a user
    $res = $user->removeUser($_GET['deleteUser']);
    if (isset($res['success'])) { // Successfully deleted ?>
      <div class="alert alert-success" role="alert">
        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
        <span class="sr-only">Bruker slettet:</span>
        Brukeren er slettet fra systemet!
      </div> <?php
    } else { // Failed deleting user ?>
      <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Feil:</span>
        Kunne ikke slette brukeren!
      </div> <?php
    }
  }
  ?>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading"><h3 class="panel-title">Legg til en ny bruker</h3></div>
      <div class="panel-body" style="margin-top: 10px;">
        <form>
          <div class="row">
            <div class="col-xs-3">
              <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input type="email" class="form-control" name="new-email" id="new-email" placeholder="E-post adresse">
              </div>
            </div>
            <div class="col-xs-3">
              <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input type="password" class="form-control" name="new-password" id="new-password" placeholder="Passord">
              </div>
            </div>
            <div class="col-xs-3">
              <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" class="form-control" name="new-firstname" id="new-firstname" placeholder="Fornavn">
              </div>
            </div>
            <div class="col-xs-3">
              <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" class="form-control" name="new-lastname" id="new-lastname" placeholder="Etternavn">
              </div>
            </div>
          </div>
          <input id="addUser" name="addUser" value="Legg til bruker" class="btn btn-primary"/>
        </form>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h3 class="panel-title">Liste over brukere av systemet</h3></div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="userTable" class="table table-striped table-hover">
            <thead>
              <th>Username</th><th width="60%">Full name</th><th style="width:30px"></th>
            </thead>
            <tbody>
              <?php
              $sql = "SELECT id, email, firstname, lastname from users ORDER BY email";
              $sth = $db->prepare ($sql);
              $sth->execute ();
              while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                echo '<tr>';
                echo "  <td><a href='mailto:{$row['email']}'>{$row['email']}</a></td><td>{$row['firstname']} {$row['lastname']}</td><td><a href='javascript: deleteUser({$row['id']}, ".'"'.$row['firstname']." ".$row['lastname'].'"'.");' title='Slett denne brukeren'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></td>";
                echo '</tr>';
              } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h3 class="panel-title">Liste over brukere som har sett dine videoer</h3></div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="userVideoTable" class="table table-striped table-hover">
            <thead>
              <th>Full name</th><th width="60%">Video</th><th style="width:30px"></th>
            </thead>
            <tbody>
              <?php
              $sql = "SELECT V.name, U.firstname, U.lastname FROM `videos` as V
              inner JOIN map_userviewed_videos as UV ON V.id = UV.vid
              inner JOIN users as U on UV.uid = U.id
              WHERE V.owner_id = ?";
              $sth = $db->prepare ($sql);
              $sth->execute (array ($user->getUID()));
              while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                echo '<tr>';
                echo " <td>{$row['firstname']} {$row['lastname']}</td><td>{$row['name']}</td>";
                echo '</tr>';
              } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <?php } else { ?>
    <div class="container">
      <div class="jumbotron">
        <h1>You are not an administrator!</h1>
        <p>Only administrators are allowed on this page</p>
      </div>
    </div>
    <?php } ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script>
    $(function () { // Make the user table sortable, searchable and paginated
      $('#userTable').dynatable();
    });

    function deleteUser (id, name) {    // Avoid accidental removal of users
      var confirm = window.confirm('Er du sikker på at du vil slette brukeren ('+name+')');
      if (confirm){
        $.ajax({
          url: 'admin.php?deleteUser='+id,
          success: function(data){
            console.log("Data: ", data);
            $('#mainContent').html(data);
          },
          error: function(error) {
            console.log("Error: ", error);
            $('#mainContent').html('resources.php?action=admin.php');
          },
          async: false
        });
      }
    }

    $('#addUser').click(function() {
      let email = $('#new-email').val();
      let password = $('#new-password').val();
      let first = $('#new-firstname').val();
      let last = $('#new-lastname').val();


      $.ajax({
        url: 'resources.php?action=admin.php',
        type: "POST",
        data: {addUser: true, newEmail: email, newPassword: password, newFirstname: first, newLastname: last},
        success: function(data){
          console.log("Data: ", data);
          $('#mainContent').html(data);
        },
        error: function(error) {
          console.log("Error: ", error);
          $('#mainContent').html('resources.php?action=admin.php');
        },
        async: false
      });
    });
    </script>
  </body>
  </html>
