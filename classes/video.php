<?php
require_once 'playlist.php';

class Video {
	var $db;
    var $id = -1;
    var $mine = false;

	/**
	 * Constructor, creates a reference to the database.
	 * Will add track to the video if post is sent.
	 */
	function Video ($db, $id=null) {
		$this->db = $db;
		if ($id!==null) {
			$this->setID($id);
			if (isset($_POST['addTrack'])&&$this->mine) {
				$this->addTrack();
			} elseif (isset($_POST['editTrack'])&&$this->mine) {
				$this->editTrack();
			}
		}
	}

    /**
		 * This function is used to set the id for the video object.
		 * Also check if the currently logged in user is owner of the specified video.
     *
     * @param id of the video
     */
    function setID ($id) {
        global $user;
    	if ($this->id!==-1)
    		return;
        $this->id = $id;
        $sql = 'SELECT owner_id FROM videos WHERE id=?';
        $sth = $this->db->prepare ($sql);
        $sth->execute (array($id));
        if ($row=$sth->fetch()) {
            $this->mine = ($row['owner_id']==$user->getUID());
        }
    }

    /**
     * Executes the sql statement and parameters passed to this method and
     * will generate a table that presents videos for the user to select
     * from.
     * @param sql the sql statement to be executed
     * @param params the parameters
     *
     *
     */
	function createVideoListTable ($sql, $params) {
        global $user;?>
		<table id="videos" class="table table-striped table-hover">
            <thead>
                </th><th style="width:200px">Tittel</th>
            </thead>
            <tbody> <?php
            	$sth = $this->db->prepare ($sql);
            	$sth->execute ($params);
            	while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {

                    // Show title
                    echo "<tr><td><b><a href='index.php?video={$row['id']}'>{$row['name']}</b><br/><span style='position: relative'></span></a></td><td data-id='{$row['id']}'>";
                    if (isset ($row['owner_id'])) {
                        echo "<a href='editVideo.php?video={$row['id']}' title='Rediger videoen'><span style='float: right;' class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>";
                    }
										echo "{$row['description']}</td></tr>";
            	} ?>
            </tbody>
        </table> <?php
	}

    /**
     * This method is used to create the video tag with source and track.
     *
     * @param id of the video to display
     */
    function createVideoTag ($id) {
        global $editing, $user, $playlist;
        $this->setID($id);
				echo '<figure id="videoContainer" data-fullscreen="false">';
        echo '<video id="video" controls preload="metadata" >';
        echo '<source src="uploads/video_'.$id.'" type="video/mp4">';
        $sql = "SELECT id, extras from videoAdditions WHERE vid=? AND mime='text/vtt'";
        $sth = $this->db->prepare($sql);
        $sth->execute (array($this->id));
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {  // Henter alle tekstene og genererer track tags
        	$track = json_decode($row['extras']);
        	echo "<track id='{$track->label}'";
        	if ($track->default)	// Om det har blitt definert en default tekst
        		echo 'default ';
  	     	echo "kind='{$track->kind}' label='{$track->label}' srclang='{$track->srclang}' src='include/getTrack.php?id={$row['id']}'/>";
        }
        echo '</video>';
				echo '<div id="video-controls" class="controls" data-state="visible">
			<button id="playpause" type="button" data-state="play">Play/Pause</button>
			<button id="stop" type="button" data-state="stop">Stop</button>
			<div class="progress">
				<progress id="progress" value="0" min="0">
					<span id="progress-bar"></span>
				</progress>
			</div>
			<button id="mute" type="button" data-state="mute">Mute/Unmute</button>
			<button id="volinc" type="button" data-state="volup">Vol+</button>
			<button id="voldec" type="button" data-state="voldown">Vol-</button>
			<button id="fs" type="button" data-state="go-fullscreen">Fullscreen</button>
			<button id="subtitles" type="button" data-state="subtitles">CC</button>
		</div>
	</figure>
	<div id="displaySub" class="subtitlesBlock">
	</div>
	<img id="subtitleImage" src="">
	<script src="./player/js/player.js"></script>
					</div>';

        if ($this->mine&&!$editing) {
            echo '<p class="bg-info">';
        }

        if ($this->mine) {  // If logged in user is owner, add option for adding to playlist
            $playlist->createAddToPlaylist();
        }
        if ($this->mine&&!$editing) {   // If owner and not editing mode, add link to edit this video
            echo '<a class="btn btn-default" href="editVideo.php?video='.$id.'">Rediger video</a> ';
        }

    }

    /**
     * This functions displays the add track form
     */
    function createUploadTrackForm () {
    	$videoid = $this->id;
    	require_once 'include/uploadTrackInfo.php';
    }

    /**
     * This method is called if the user tries to uploaded a track
     * for this video object.
     */
    function addTrack () {
    	$sql = 'INSERT INTO videoAdditions (vid, mime, extras, content) VALUES (?, "text/vtt", ?, ?)';
        // Information about the text track is stored as json encoded data
    	$extras = json_encode(array ('default'=>isset($_POST['default']), 'kind'=>$_POST['kind'], 'label'=>$_POST['label'], 'srclang'=>$_POST['srclang']));
    	$content = "";
    	if (is_uploaded_file($_FILES['file']['tmp_name'])) {   // A file has been uploaded
    		$content = file_get_contents($_FILES['file']['tmp_name']);
    	}
    	$sth = $this->db->prepare ($sql);
        // Store the text track in the database
    	$sth->execute (array ($_GET['video'], $extras, $content));
    }

		/**
     * This method is called if the user edits the track of the
		 * current video object
     */
    function editTrack () {
    	$sql = 'UPDATE videoAdditions SET mime="text/vtt", extras=?, content=? WHERE id=?';
        // Information about the text track is stored as json encoded data
    	$extras = json_encode(array ('default'=>isset($_POST['default']), 'kind'=>$_POST['kind'], 'label'=>$_POST['label'], 'srclang'=>$_POST['srclang']));
			$content = $_POST['editorText'];
    	$sth = $this->db->prepare ($sql);
        // Store the text track in the database
    	$sth->execute (array ($extras, $content, $_POST['id']));
    }
}

if (isset($_GET['video']))      // We are on a page that shows a video
	$video = new Video($db, $_GET['video']);
else
	$video = new Video($db);   // The object is to be used for other tasks than displaying a video
