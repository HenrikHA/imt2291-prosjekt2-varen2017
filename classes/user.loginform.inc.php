<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Logg inn</div>
                <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Glemt passordet?</a></div>
            </div>

            <div style="padding-top:30px" class="panel-body" >

				<?php echo $this->alert; ?>

                <form id="loginform" class="form-horizontal" role="form">
                  <div style="margin-bottom: 25px" class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                      <select id="login-accountType" type="accountType" class="form-control" name="accountType" required>
                        <option value="admin">Administrator</option>
                        <option value="student">Student</option>
                      </select>
                  </div>
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input id="login-username" type="email" class="form-control" name="email" <?php echo $email; ?> placeholder="E-post adressen din" required>
                    </div>
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="password" placeholder="Passord" required>
                    </div>
                    <div class="input-group">
                        <div class="checkbox">
                            <label>
                                <input id="login-remember" type="checkbox" name="remember" value="1"> Husk meg
                            </label>
                        </div>
                    </div>

                    <div style="margin-top:10px" class="form-group">
                        <div class="col-sm-12 controls">
                            <input id="btn-login" class="btn btn-success" value="Login"/>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$('#btn-login').click(function() {
  let email = $('#login-username').val();
  let password = $('#login-password').val();
  let remember = $('#login-remember').val();


  $.ajax({
    url: 'resources.php?action=login.php',
    type: "POST",
    data: {email: email, password: password, remember: remember},
    success: function(data){
      console.log("Data: ", data);
      $('#mainContent').html(data);
      // Update topmenu
      updateNavbar();
    },
    error: function(error) {
      console.log("Error: ", error);
      $('#mainContent').html('resources.php?action=login.php');
    },
    async: false
  });
});

</script>
