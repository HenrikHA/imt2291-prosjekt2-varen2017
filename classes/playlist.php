<?php

require_once 'include/db.php';

class Playlist {
	var $db;

	/**
	 * Takes a reference to the database as a parameter.
	 * If $_POST['newPlaylistName'] exists a new playlist will be created.
	 * If $_POST['addToPlaylist'] exists the a video will be added to selected playlist.
	 *
	 * @param db a reference to the database object
	 */
	function Playlist ($db) {
		$this->db = $db;
		if (isset ($_POST['newPlaylistName'])) {
			// Oppretter ny spilleliste
			$this->createPlaylist($_POST['newPlaylistName']);
		} else if (isset ($_POST['addToPlaylist'])) {
			// Legger video til en spilleliste
			$this->addToPlaylist($_GET['video'], $_POST['addToPlaylist'], isset($_POST['isThumbnail']));
		}
	}

	/**
	 * This method is used to generate a playlist table.
	 *
	 * @param sql the sql expression to execute
	 * @param param the parameters
	 */
	function generatePlaylist ($sql, $param) {
		global $user; ?>
    	<table id="playlists" class="table table-striped table-hover">
            <thead>
                <th>Tittel</th>
            </thead>
            <tbody> <?php
            	$sth = $this->db->prepare ($sql);
            	$sth->execute ($param);
            	while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            		echo "<tr><td><b><a href='playlist.php?id={$row['id']}'>{$row['name']}<b><br/></a></td><td>";
            		if (isset($row['owner_id'])) {
            			echo "<a href='editPlaylist.php?id={$row['id']}' title='Rediger spilleliste'><span style='float: right;' class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>";
            		}
            		echo "</td></tr>";
            	} ?>
            </tbody>
        </table> <?php
	}

	/**
	 * This method is used to create a new playlist.
	 * Name of the playlist to be created as a parameter
	 * The newly created playlist will belong to the user who calls this function.
	 *
	 * @param name the name of the playlist to create.
	 */
	function createPlayList ($name) {
		global $user;
		if ($user->isLoggedIn()) {
			$sql = "INSERT INTO playlists (owner_id, name) VALUES (?, ?)";
			$sth = $this->db->prepare ($sql);
			$sth->execute (array ($user->getUID(), $name));
		}
	}

	/**
	 * This method is used to create a "add to playlist option"
	 *
	 */
	function createAddToPlaylist () {
		global $user;
        $sql = 'SELECT id, name FROM playlists WHERE owner_id=?';
        $sth = $this->db->prepare ($sql);
        $sth->execute (array ($user->getUID()));
        if ($row = $sth->fetch()) {
					echo '<div class="container-fluid" style="margin-top: 10px"><div class="row">';
					echo '<div class="col-xs-12 col-lg-6 col-lg-offset-3">';
            echo '<form class="form-inline" method="post" action="" style="float: right"><div class="form-group"><select style="width:200px;" id="addToPlaylist" class="form-control" name="addToPlaylist">';
            echo "<option value='{$row['id']}'>{$row['name']}</option>";
            while ($row = $sth->fetch()) {
                echo "<option value='{$row['id']}'>{$row['name']}</option>";
            }
            echo '</select></div>';
            echo '&nbsp; <input type="submit" value="Legg til i spilleliste" class="btn btn-primary"></form></div></div>';
        }
	}

	/**
	 * This method is used to add a video to a playlist
	 *
	 * @param video the id of the video to add
	 * @param playlist the id of the playlist to add this video to
	 */
	function addToPlaylist($video, $playlist, $isThumbnail) {
		global $user;

		// sorder will be 1 higher than the highest within a certain playlist
		$sql = "INSERT INTO map_videos_playlists (vid, pid, sorder) select ?, ?, MAX(sorder)+1 FROM map_videos_playlists WHERE pid=?";
		$sth = $this->db->prepare ($sql);
		$sth->execute (array ($video, $playlist, $playlist))
		// The video was successfully added to the playlist
		?>
		<div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only">Suksess:</span>
            Videoen er lagt til i spillelisten
        </div>
        <script>
        	$(function() {
        		// Fade the feedback out and then remove it
        		$('div[role="alert"]').fadeOut(5000);
        	});
        </script> <?php
	}
}

$playlist = new Playlist($db);
