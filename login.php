<?php
//session_start();
require_once 'include/db.php';
require_once 'classes/user.php';    // Will make sure the user gets logged in
if ($user->isLoggedIn()) {
  require_once('include/newVideos.php');
} else {
  $user->getLoginForm();  // Show the login form
}

?>
