<?php

$host = 'localhost';
$root = 'root';
$rootPassword = '';

// Info for the new user and database
$user = 'imt2291-2';
$password = 'imt2291-2';
$dbname = 'prosjekt2';

try {
  // Creates the new database and user
  $dbh = new PDO("mysql:host=$host", $root, $rootPassword);

  $dbh->exec("CREATE DATABASE IF NOT EXISTS `$dbname`;
    CREATE USER '$user'@'localhost' IDENTIFIED BY '$password';
    GRANT ALL ON `$dbname`.* TO '$user'@'localhost';
    FLUSH PRIVILEGES;")
    or die (print_r($dbh->errorInfo(), true));

  // Importes the sql file
  $dbh = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
  $query = file_get_contents('database.sql');
  $stmt = $dbh->prepare($query);

  if ($stmt->execute()) {
    echo 'Database installation succeeded!';
  } else {
    echo 'Database installation failed!';
  }

  } catch(PDOException $e) {
    die('DB ERROR: '. $e->getMessage());
  }
